FROM pommalabs/dotnet:9-aspnet

# Move to tmp directory, which will be cleaned after everything has been installed.
# Moreover, switch to root user in order to have a full grants.
WORKDIR /tmp
USER root

# Thumbnailer started using an application user ID equal to 1000.
# Microsoft, as of .NET 8, introduced a default application user
# whose ID is different than 1000, but Thumbnailer needs to keep using previous ID.
# In fact, existing installations might have temporary directories
# and data protection keys whose permissions are configured for previous user ID.
ENV APP_UID="1000"
RUN usermod -u ${APP_UID} app && groupmod -g ${APP_UID} app

# Add contrib and backports repositories.
COPY config/debian.sources /etc/apt/sources.list.d/debian.sources

# Copy additional fonts and font configurations.
COPY fonts/ppviewer /usr/share/fonts/truetype/ppviewer
COPY config/fonts.conf /etc/fonts/conf.d/100-thumbnailer.conf

# Execute setup script, which will delete itself during its own cleanup phase.
COPY scripts/setup.sh .
RUN chmod +x setup.sh && ./setup.sh

# Grant full access on Thumbnailer directory inside /tmp to application user.
RUN chown app:app /tmp/thumbnailer

# Evaluator simply wraps a bash eval command.
# It is used to perform redirections and other operations
# which would be easy in bash but not so easy in .NET.
COPY scripts/evaluator.sh /usr/local/bin/evaluator
RUN chmod +x /usr/local/bin/evaluator

# Let ImageMagick use the same temporary directory used by application code, 
# so that its temporary files are cleaned up by existing application cleanup flows.
# See: https://imagemagick.org/script/resources.php
ENV MAGICK_TEMPORARY_PATH="/tmp/thumbnailer"

# Force paper size to be A4, so that paper size used by default by LibreOffice is fixed.
# Locale "en_GB.UTF-8", generated inside setup.sh, has a default A4 paper size.
ENV LC_PAPER="en_GB.UTF-8"

# Switch back to application directory and unprivileged user.
WORKDIR /opt/app
USER $APP_UID
