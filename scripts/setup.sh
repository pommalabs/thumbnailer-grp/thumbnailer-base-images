#!/bin/bash
set -e

DEBIAN_FRONTEND="noninteractive"
SLIM_INSTALL="apt-get install -y -qq --no-install-recommends -t bookworm-backports"

# Update all repositories.
apt-get update -qq

# Install additional software used by the application.
#
# calibre:           Thumbnails of EPUB files.
# dcmtk:             Thumbnails of DCM files.
# dcraw:             Thumbnails of RAW files.
# ffmpeg:            Thumbnails of MP4 and video files.
# firefox-esr:       Thumbnails of HTML files.
# ghostscript:       Thumbnails of PDF files.
# gifsicle:          Optimization of GIF files.
# imagemagick:       Thumbnails of PSD files.
# jpegoptim:         Optimization of JPEG files.
# libvips42:         Thumbnail generation, image manipulation and smart cropping.
# mhonarc:           Thumbnails of EML files.
# pngquant:          Optimization of PNG files.
# python3-pygments:  Thumbnails of text files and source code.
# scour:             Optimization of SVG files.
# webp:              Optimization of WEBP files.
$SLIM_INSTALL \
    calibre dcmtk dcraw ffmpeg firefox-esr ghostscript gifsicle imagemagick \
    jpegoptim libvips42 mhonarc pngquant python3-pygments scour webp    

# Install LibreOffice, used to create thumbnails of DOCX, XLSX, PPTX
# and other Office/LibreOffice files. Explicit installation of Java support
# is required in order to avoid runtime error messages.
$SLIM_INSTALL default-jre libreoffice libreoffice-java-common

# Remove ImageMagick security policy for file types handled by Ghostscript,
# as the vulnerability for which the policy was created (https://www.kb.cert.org/vuls/id/332928/)
# has been addressed in Ghostscript version 9.24. 
sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml

# Verify installed executables.
cwebp -version
dcmj2pnm --version
ebook-convert --version
ffmpeg -version
firefox --version
ghostscript --version
gifsicle --version
hash dcraw
jpegoptim --version
libreoffice --version
mhonarc -v
pngquant --version
pygmentize -V
scour --version

# Install Microsoft fonts.
$SLIM_INSTALL fontconfig ttf-mscorefonts-installer

# Install Debian fonts.
# Credits: https://github.com/gotenberg/gotenberg/blob/main/build/Dockerfile
$SLIM_INSTALL \
    culmus \
    fonts-arphic-ukai \
    fonts-arphic-uming \
    fonts-beng \
    fonts-crosextra-caladea \
    fonts-crosextra-carlito \
    fonts-dejavu \
    fonts-droid-fallback \
    fonts-freefont-ttf \
    fonts-hosny-amiri \
    fonts-ipafont \
    fonts-kacst \
    fonts-liberation \
    fonts-liberation2 \
    fonts-linuxlibertine \
    fonts-lklug-sinhala \
    fonts-lohit-guru \
    fonts-lohit-knda \
    fonts-noto \
    fonts-opensymbol \
    fonts-sarai \
    fonts-samyak \
    fonts-sil-abyssinica \
    fonts-sil-gentium \
    fonts-sil-padauk \
    fonts-telu \
    fonts-thai-tlwg \
    fonts-unfonts-core \
    fonts-wqy-zenhei \
    gsfonts \
    ttf-bitstream-vera

# Refresh font cache.
fc-cache -fv

# Force paper size to be A4, so that paper size used by default by LibreOffice is fixed.
# A new locale, "en_GB.UTF-8", is prepared to be used with LC_PAPER environment variable,
# because paper size might also be read from configured locale.
echo "a4" | tee /etc/papersize
$SLIM_INSTALL locales
echo "en_GB.UTF-8 UTF-8" | tee --append /etc/locale.gen
locale-gen
apt-get remove -y -qq locales

# Cleanup unused APT packages, APT cache and temporary files.
apt-get autoremove -y -qq
rm -rf /var/lib/apt/lists/* /tmp/*

# Create directory for Thumbnailer temporary files.
mkdir /tmp/thumbnailer
